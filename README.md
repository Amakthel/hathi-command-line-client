# hathi-command-line-client

A command-line client for the purpose of quick prototyping of features

# Running the proto-client

Get a hathi server running as listed in the README for hathi-server.

Once that's running, all current uses of the proto-client will look like

```
./proto-client.py
```
followed by some number of flags and arguments.

# Available flags

```
--actor
--bcc
--cc
--create-actor
--ping
--port
--request
--send-message
--server
--test
--to
--topic
--v
--vv
--vvv
```

# Current capabilities of the proto-client

For any of the following uses, you have the option of adding the --v, --vv,
and --vvv flags to the end of the command for the minimally verbose, verbose,
and maximally verbose options. The commands run as with --v by default. --vv
and --vvv typically only introduce connection data and visualization of the
JSON sent.

## Testing

To run the default test suite under /tests, delete the server database, restart
the server, and run:
```
./proto-client.py --test
```
A successful test run will look like
```
Running Test Suite
...
---------------------------------------------------------------------
Ran 3 tests in 0.091s

OK
```

The exact number of seconds isn't important, but it should be small, like
above.

## Adding actors

To add an Actor to the currently active server and its database, you run the
following:
```
./proto-client.py --create-actor "CirceTheCircular"
```
You should see a line saying
```
Actor CirceTheCircular created.
```
if you are successful. You can replace CirceTheCircular with whatever name you
wish for your Actor. We'll use CirceTheCircular for other examples, but you can
replace her with the Actor that is relevant to your case.

## Sending messages

To send a message from one Actor to another, use the following:
```
./proto-client.py --actor "sender" --to "recipient" --topic "title of message" --send-message "This is the body of the message"
```

Optionally, you can use flags such as --bcc and --cc to send blind carbon
copies or regular carbon copies of a message, in addition to the original. The
flags take a string naming the Actor to whom you wish to send the blind carbon
copy or carbon copy.

## Retrieving messages

Retrieving the messages in your inbox is a two step process. The object with
the ID /relevantActor/inbox is a JSON object that contains information about
the inbox, including the object-ids of the JSON objects that are the messages
themselves. Let's show you how to request an object.

Object requsts are of the form
```
./proto-client.py --request '{"version":1, "request":1, "function":"get-object", "object-id":"/id/of/your/object"}'
```
in our case, we're looking for CirceTheCircular's inbox, so the object-id is
/CirceTheCircular/inbox. Like this:
```
./proto-client.py --request '{"version":1, "request":1, "function":"get-object", "object-id":"/CirceTheCircular/inbox"}'
```
This will return a blob of JSON. Look at that blob, under "object", and within
that, under "items". There will be some number of strings there, all starting
with "http://" . These are the messages' ids, and you can request the message
just as above by using --request with their id under "object-id".
