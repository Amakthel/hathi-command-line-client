#!/usr/bin/python3

import socket
import json
import logging
import unittest
from urllib.parse import urlparse
import argparse
import sys
from time import sleep, strftime


def prettify(obj):
    """
    Format and indent object data.

    For use with eg. pretty-printing in order to present standardized output.
    """
    return json.dumps(obj, sort_keys=True, indent=4)


def json_prettify(json_text):
    """
    Format and indent json text data.

    For use with eg. pretty-printing in order to present standardized output.
    """
    json_data = json.loads(json_text)
    return prettify(json_data)


def load_json_file(filepath):
    """Load initialization data from file"""
    try:
        data_file = open(filepath, 'r')
        data = json.loads(data_file.read())
    finally:
        data_file.close()
    return data


def is_url(url):
    """Tests if a text string is a valid url or not"""
    try:
        urlparse(url)
        return True
    except ValueError:
        return False


class EndlessLoopException(Exception):
    pass


class RequestID:
    """
    Generates new request ids for use when constructing requests

    The client should send a unique id with each request, so that
    it can later recognize which response belongs to which request,
    in case of having sent multiple requests at a time.
    RequestID is for generating new, unique ids.
    """
    def __iter__(self):
        self.request_id = 1
        return self

    def __next__(self):
        if self.request_id <= 999:
            request_id = self.request_id
            self.request_id += 1
            return request_id
        else:
            # While we may at one time need a client to make
            # thousands of requests at a time, for ICN version the
            # more likely scenario is that someone somewhere made
            # a mistake
            logging.warning("Number of requests are above expected values")
            raise StopIteration


class TCPSocket:
    """
    TCP socket interface.

    Sends and receives json structures to and from a remote server.
    Both input and output are generally assumed to be python objects,
    and this is what the main request(obj) function works with.
    Use:
    Initialize with hostname and port, then use the request(obj) function
    to communicate with the remote server.
    """
    def __init__(self, host, port):
        self.host = host
        self.port = port
        self.tcp_socket = None
        self.chunk_length = 32

    def connect(self):
        self.tcp_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        logging.info('Connecting to {} port {}'.format(self.host, self.port))
        self.tcp_socket.connect((self.host, self.port))

    def close(self):
        logging.info('Closing socket')
        self.tcp_socket.close()
        self.tcp_socket = None

    def send(self, message):
        logging.info("Sending request")
        logging.debug(json_prettify(message))
        bytestream = message.encode()
        self.tcp_socket.sendall(bytestream)

    def receive(self):
        logging.info("Receiving response")
        socket_open = True
        received_stream = "".encode()       # Starting with empty bytestream
        loop_count = 0
        while socket_open:
            loop_count += 1
            if loop_count > 100:
                raise EndlessLoopException("Unclosed socket")
            data_chunk = self.tcp_socket.recv(self.chunk_length)
            received_stream += data_chunk

            # According to standard, empty bytestream is considered EOF
            # TODO: More testing, as it seems no empty bytestream is ever
            # received; detecting on length is a fix.
            # Potential issue:
            # What happens if received message-length has
            # (modulus <self.chunk_length> == 0) ?
            if data_chunk == b'' or len(data_chunk) < self.chunk_length:
                socket_open = False
        received = received_stream.decode()
        logging.debug(json_prettify(received))
        return received

    def msg_request(self, request_msg):
        try:
            self.connect()
            # Since we are client, communication will always happen by us
            # initiating a request, and the server responding.
            # Refactor:
            # Send and receive functions should possibly only be defined
            # inside msg_request since they are dependent upon each other
            # and have no purpose outside of msg_request.
            self.send(request_msg)
            response = self.receive()
        except OSError as ose:
            logging.error("Error #{}: {}".format(ose.errno, ose.strerror))
            if ose.errno == 111:   # Connection refused
                sys.exit(2)
        except EndlessLoopException as ele:
            logging.error(ele)
        finally:
            self.close()
        return response

    def request(self, request_obj):
        request_msg = json.dumps(request_obj)
        response_msg = self.msg_request(request_msg)
        response_obj = json.loads(response_msg)
        return response_obj


class TestCaseBasicFunction(unittest.TestCase):
    # Docstrings avoided since they would be displayed by the
    # unittest testing system as part of the test run.
    #
    # Testing class; performs unit testing to ensure basic
    # functionality.

    def __init__(self, testname, host, port):
        super().__init__(testname)
        self.host = host
        self.port = port

    def setUp(self):
        self.test_socket = TCPSocket(self.host, self.port)

#    def tearDown(self):
#        [code to execute to clean up after tests]

    def test_create_actor(self):
        request = {
            "version": 1,
            "request": 1,
            "function": "new-actor",
            "actor": "/actor-create-test"
        }
        logging.info("Request:\n%s\n", prettify(request))
        response = self.test_socket.request(request)
        logging.info("Response:\n%s\n", prettify(response))
        try:
            assert(response["version"] == 1), "Version mismatch"
            assert(response["request"] == 1), "Request id mismatch"
            assert(response["function"] == "result"), "Not a result"
            assert(response["actor"] == "/actor-create-test"),\
                "Actor not received"
        except AssertionError as ae:
            logging.error(ae)

    def test_get_object(self):
        request = {
            "version": 1,
            "request": 2,
            "function": "get-object",
            "object-id": "/actor-create-test/inbox"
        }
        result_obj = {
            "@id": "/actor-create-test/inbox",
            "@type": "OrderedCollection"
        }
        logging.info("Request:\n%s\n", prettify(request))
        response = self.test_socket.request(request)
        logging.info("Response:\n%s\n", prettify(response))
        try:
            assert(response["version"] == 1), "Version mismatch"
            assert(response["request"] == 2), "Request id mismatch"
            assert(response["function"] == "result"), "Not a result"
            assert(response["object"] == result_obj), "Unexpected result"
        except AssertionError as ae:
            logging.error(ae)

    def test_post_outbox(self):
        request = {
            "version": 1,
            "request": 3,
            "function": "post-outbox",
            "actor": "/actor-create-test",
            "object": {
                "@type": "Create",
                "object": {
                    "@type": "Note",
                    "summary": "test",
                    "content": "blah blah"
                }
            }
        }
        logging.info("Request:\n%s\n", prettify(request))
        response = self.test_socket.request(request)
        logging.info("Response:\n%s\n", prettify(response))
        try:
            assert(response["version"] == 1), "Version mismatch"
            assert(response["request"] == 3), "Request id mismatch"
            assert(response["function"] == "result"), "Not a result"
            assert(is_url(response["object-id"])), "Unexpected result"
        except AssertionError as ae:
            logging.error(ae)


# Temporary class which will fairly soon be refactored into a library module.
# All functions should be able to run independently to facilitate this future.
class ClientLibrary:
    def __init__(self, host, port):
        self.version = 1
        self.request_id = iter(RequestID())
        self.host = host
        self.port = port
        self.tcpsocket = TCPSocket(self.host, self.port)

    def gen_version(self):
        return ("version", self.version)

    def gen_request(self):
        return ("request", next(self.request_id))

    def gen_function(self, function):
        return ("function", function)

    # todo: validate name string
    def gen_actor(self, actor_name):
        return ("actor", "/" + actor_name)

    # todo: validate name strings
    def gen_actor_list(self, actor_names):
        actor_list = ["/" + name for name in actor_names]
        logging.debug(str(actor_list))
        return actor_list

    def gen_type(self, type_str):
        return ("@type", type_str)

    def gen_summary(self, summary):
        return ("summary", summary)

    def gen_to(self, to_actors):
        result = ("to", self.gen_actor_list(to_actors))
        logging.debug(prettify(dict([result])))
        return result

    def gen_cc_actors(self, cc_actors):
        result = ("cc", self.gen_actor_list(cc_actors))
        logging.debug(prettify(dict([result])))
        return result

    def gen_bcc_actors(self, bcc_actors):
        result = ("bcc", self.gen_actor_list(bcc_actors))
        logging.debug(prettify(dict([result])))
        return result

    def gen_content(self, content):
        return ("content", content)

    def gen_object(self, values):
        result = ("object", values)
        logging.debug(prettify(dict([result])))
        return result

    def gen_note(self, summary, content):
        kv_type = self.gen_type("Note")
        kv_summary = self.gen_summary(summary)
        kv_content = self.gen_content(content)
        dict_values = dict([kv_type, kv_summary, kv_content])
        kv_object = self.gen_object(dict_values)
        logging.debug(prettify(dict([kv_object])))
        return dict([kv_object])

    def ping(self):
        function = self.gen_function("ping")
        request = dict([self.gen_version(), self.gen_request(), function])
        print(request)
        while True:
            logging.info("Request:\n%s\n", prettify(request))
            print("{}s: Ping".format(strftime("%x %X")))
            response = self.tcpsocket.request(request)
            logging.info("Response:\n%s\n", prettify(response))
            if response["function"] == "pong":
                print("{}s: Pong".format(strftime("%x %X")))
            sleep(1)

    # todo: handle potential errors like actor already existing
    def create_actor(self, actor_name):
        function = self.gen_function("new-actor")
        actor = self.gen_actor(actor_name)
        request = dict([self.gen_version(), self.gen_request(),
                        function, actor])
        logging.info("Request:\n%s\n", prettify(request))
        response = self.tcpsocket.request(request)
        logging.info("Response:\n%s\n", prettify(response))
        if response["actor"] == ("/" + actor_name):
            print("Actor {} created.".format(actor_name))
        else:
            print("Something went wrong.")
            print(response["actor"])
            print(actor_name)

    # todo: multiple recipents, message options; possibly bcc's?
    def send_message(self, from_actor, summary, content,
                     to_actor, cc_actor, bcc_actor):
        kv_function = self.gen_function("post-outbox")
        kv_from_actor = self.gen_actor(from_actor)
        kv_to_actors = self.gen_to([to_actor])
        kv_note = self.gen_note(summary, content)
        logging.debug(prettify(kv_note))
        kv_note["object"].update([kv_to_actors])
        if cc_actor:
            kv_cc_actors = self.gen_cc_actors([cc_actor])
            kv_note["object"].update([kv_cc_actors])
        if bcc_actor:
            kv_bcc_actors = self.gen_bcc_actors([bcc_actor])
            kv_note["object"].update([kv_bcc_actors])
        logging.debug(prettify(kv_note))
        request = dict([self.gen_version(), self.gen_request(),
                        kv_function, kv_from_actor])
        request.update(kv_note)
        logging.info("Request:\n%s\n", prettify(request))
        response = self.tcpsocket.request(request)
        logging.info("Response:\n%s\n", prettify(response))
        # if response["actor"] == ("/" + actor_name):
        #    print("Actor {} created.".format(actor_name))
        # else:
        #    print("Something went wrong.")
        #    print(response["actor"])
        #    print(actor_name)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v", "--verbosity", action="count", default=0,
                        help="Verbosity level; v to vvv")
    parser.add_argument("--test", help="Run test suite",
                        action="store_true")
    parser.add_argument("--ping", help="Ping server",
                        action="store_true")
    parser.add_argument("--server", default=socket.gethostname(),
                        help="Server to connect to; defaults to localhost")
    parser.add_argument("--port", type=int, default=8080,
                        help="Port to connect to; defaults to 8080")
    parser.add_argument("--request",
                        help="Direct send of supplied text "
                        "to remote server for testing purposes.")
    parser.add_argument("--create-actor",
                        help="Create an actor with the given name.")
    parser.add_argument("--send-message",
                        help="Send a message (text string argument for now).")
    parser.add_argument("--actor",
                        help="Use this actor to perform the command given.")
    parser.add_argument("--to",
                        help="Recipient of something.")
    parser.add_argument("--cc",
                        help="Recipient of carbon copy of something.")
    parser.add_argument("--bcc",
                        help="Recipient of blind carbon copy of something.")
    parser.add_argument("--topic",
                        help="Topic of a message.")
    args = parser.parse_args()

    # Setting logging verbosity
    # Logging level can be one of DEBUG|INFO|WARNING|ERROR|CRITICAL
    if args.verbosity == 0:
        logging_level = logging.ERROR
    elif args.verbosity == 1:
        logging_level = logging.WARNING
    elif args.verbosity == 2:
        logging_level = logging.INFO
    elif args.verbosity >= 3:
        logging_level = logging.DEBUG
    logging_format = \
        '%(levelname)-8s - '\
        '%(filename)-10s | '\
        '%(funcName)-8s | '\
        'line %(lineno)3d: '\
        '%(message)s'
    logging.basicConfig(level=logging_level, format=logging_format)
    client_lib = ClientLibrary(args.server, args.port)

    if args.test:
        print("Running test suite")

        # Jumping some hoops instead of calling unittest.main()
        # Doing so allows us to pass server and port as arguments
        # to the test suite's constructor and thus the setUp function.
        test_loader = unittest.TestLoader()
        test_names = test_loader.getTestCaseNames(TestCaseBasicFunction)
        suite = unittest.TestSuite()
        for test_name in test_names:
            suite.addTest(TestCaseBasicFunction(test_name,
                                                args.server,
                                                args.port))
        result = unittest.TextTestRunner().run(suite)
        sys.exit(not result.wasSuccessful())
    elif args.ping:
        client_lib.ping()
    elif args.request:
        tcpsocket = TCPSocket(args.server, args.port)
        print(args.request)
        logging.info("Request:\n%s\n", json_prettify(args.request))
        response = json.loads(tcpsocket.msg_request(args.request))
        logging.info("Response:\n%s\n", prettify(response))
        print(prettify(response))
    elif args.create_actor:
        client_lib.create_actor(args.create_actor)
    elif args.send_message:
        if not args.actor:
            print("No actor argument given.")
        elif not args.to:
            print("No recipient to send to.")
        else:
            if not args.topic:
                topic = "None"
            else:
                topic = args.topic
            logging.debug("From: {}".format(args.actor))
            logging.debug("To: {}".format(args.to))
            logging.debug("Topic: {}".format(topic))
            logging.debug("Message: {}".format(args.send_message))
            client_lib.send_message(args.actor, topic, args.send_message,
                                    args.to, args.cc, args.bcc)
    else:
        print("Menu unimplemented")


# Script entry point; simply runs main() function
# The condition is so that if someone loads this file as a module it is only
# the loading that happens, and we avoid executing the main function.
if __name__ == '__main__':
    main()

# Todo:
# Implement functionality such as ping, sending and receiving messages..
